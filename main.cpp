#include <QDebug>
#include <QFile>
#include <QDir>
#include <QNetworkReply>
#include <QWebPage>
#include <QWebFrame>
#include <QWebElement>
#include <QCryptographicHash>
#include <QScriptEngine>
#include <QScriptValue>
#include <QScriptValueIterator>
#include <QApplication>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);

	if (argc<2)
	{
		qWarning() << "Wrong number of arguments!";
		qWarning() << "Usage:";
		qWarning() << QString("%1 <file_name> <output_dir>").arg(argv[0]);
		return -1;
	}

	QFile file(argv[1]);
	if (!file.open(QIODevice::ReadOnly))
	{
		qWarning() << "Cannot open file for reading!";
		qWarning() << "Error = " << file.error();
		return -1;
	}

	qDebug() << "Reading HTML...";
	QByteArray input(file.readAll());
	file.close();
	qDebug() << "Read! content size=" << input.size();

	QString outputDirName = argv[2];
	QDir outputDir;
	if (!outputDir.cd(outputDirName))
	{
		if (!outputDir.mkdir(outputDirName))
		{
			file.close();
			qWarning() << "Cannot create output dir!";
			return -1;
		}
		if (!outputDir.cd(outputDirName))
		{
			file.close();
			qWarning() << "Cannot switch to output dir!";
			return -1;
		}
	}

	qDebug() << "Output dir:" << outputDir.absolutePath();

	if (!outputDir.cd("tmp"))
	{
		if (!outputDir.mkdir("tmp"))
		{
			file.close();
			qWarning() << "Cannot create temporary dir!";
			return -1;
		}
		if (!outputDir.cd("tmp"))
		{
			file.close();
			qWarning() << "Cannot switch to temporary dir!";
			return -1;
		}
	}

	QString prefix("src=\"data:image/png;base64,");
	QByteArray processed;

	int first = 0;
	for (int last=input.indexOf(prefix, first); last!=-1; last=input.indexOf(prefix, first))
	{
		int begin=last+prefix.size();
		last+=5;
		int end=input.indexOf('\"', begin);
		QByteArray bin = QByteArray::fromBase64(input.mid(begin, end-begin));
		QCryptographicHash hash(QCryptographicHash::Sha1);
		hash.addData(bin);
		QString fileName = hash.result().toHex()+".png";
		QFile image(outputDir.filePath(fileName));
		if (image.open(QIODevice::WriteOnly|QIODevice::Truncate))
		{
			image.write(bin);
			image.close();
		}
		else
			qWarning() << "Can't open file for writing:" << fileName;

		QByteArray out = input.mid(first, last-first);

		processed.append(out);
		QUrl url = QUrl::fromLocalFile(image.fileName());
		processed.append(url.toEncoded());

		first=end;
	}

	QByteArray out = input.mid(first, -1);
	processed.append(out);

	qDebug() << "Processed! size=" << processed.size();

	outputDir.cdUp();

	qDebug() << "Reading JSON...";

	QStringList supportedEmojis;

	file.setFileName("emoji.json");
	if (file.open(QIODevice::ReadOnly))
	{
		QByteArray json = file.readAll();
		file.close();
		if (!json.isEmpty())
		{
			QScriptEngine engine;
			QString js("("+QString::fromUtf8(json)+")");

			QScriptValue value = engine.evaluate(js);
			if (value.isValid())
			{
				QScriptValueIterator it(value);
				do
				{
					it.next();

					QScriptValue val = it.value();

					QString unicode = val.property("unicode").toString();
					supportedEmojis.append(unicode);
					qDebug() << "found:" << unicode;
					unicode = val.property("unicode_alternates").toString();
					if (!unicode.isEmpty())
					{
						supportedEmojis.append(unicode);
						qDebug() << "found:" << unicode;
					}
				} while (it.hasNext());
			}
		}
	}
	else
	{
		qWarning() << "Cannot open emoji.json for reading!";
		qWarning() << "Error = " << file.error();
		return -1;
	}

	QStringList names;
	names << "chart" << "apple" << "google" << "twitter" << "one" << "fb" << "fbm" << "samsung" << "windows";

	QWebPage page;
	QWebFrame *frame = page.mainFrame();

	frame->setContent(processed);

	QWebElement root = frame->documentElement();

	QWebElement body;
	for (QWebElement element = root.firstChild(); !element.isNull(); element=element.nextSibling())
		if (element.tagName()=="BODY")
		{
			body = element;
			break;
		}

	qDebug() << "Extarcting emoji";

	QWebElement tbody;
	for (QWebElement element = body.firstChild(); !element.isNull(); element=element.nextSibling())
		if (element.tagName()=="DIV")
			for (QWebElement e = element.firstChild(); !e.isNull(); e=e.nextSibling())
				if (e.tagName()=="TABLE")
				{
					tbody = e.firstChild();
					break;
				}

	if (tbody.tagName()=="TBODY")
	{
		QDir dir;
		if (argc>2)
		{
			bool ok;
			ok = dir.cd(argv[2]);
			if (!ok && dir.mkdir(argv[2]))
				ok = dir.cd(argv[2]);
			if (!ok)
			{
				qWarning() << "Cannot use output dir specified";
				return -3;
			}
		}

		for (QWebElement tr = tbody.firstChild(); !tr.isNull(); tr=tr.nextSibling())
			if (tr.tagName()=="TR")
			{
				QString fileName;
				int i=0;
				bool ok = true;
				for (QWebElement td = tr.firstChild(); !td.isNull() && i<12; td=td.nextSibling(), ++i)
					if (td.tagName()=="TD" && i!=6)
					{						
						if (td.classes().first()=="chars")
						{
                            QVector<uint> ucs4 = td.toInnerXml().toUcs4();
                            for (QVector<uint>::ConstIterator it=ucs4.constBegin(); it!=ucs4.constEnd() && *it; ++it)
							{
								if (!fileName.isEmpty())
									fileName.append('-');
								fileName.append(QString::number(*it, 16));
							}
							if (!supportedEmojis.contains(fileName))
							{
//								qDebug() << "Not supported:" << fileName;
								ok = false;
								break;
							}
							fileName.append(".png");							
                        }
						else
						{
							if (td.classes().contains("andr"))
							{
								QWebElement img = td.firstChild();
								if (img.tagName()=="IMG")
								{
									bool ok = dir.cd(names.at(i-3));
									if (!ok && dir.mkdir(names.at(i-3)))
										ok = dir.cd(names.at(i-3));

									if (ok)
									{
										QFile file(QUrl::fromEncoded(img.attribute("src").toLatin1()).toLocalFile());
										file.rename(dir.absoluteFilePath(fileName));
										dir.cdUp();
									}
									else
										qWarning() << "Failed to change directory!";
								}
							}
						}
					}

				if (!ok)
					continue;
			}
			else
				qDebug() << "tagName=" << tr.tagName();
		qDebug() << "All done!";
	}
	else
	{
		qDebug() << "Wrong element found:" << tbody.tagName();
		return -4;
	}

	return 0;
}
